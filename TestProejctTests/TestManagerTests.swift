//
//  TestManagerTests.swift
//  TestProejctTests
//
//  Created by Foxconn_Civet_MacMini on 2021/10/20.
//

import XCTest
@testable import TestProejct
class TestManagerTests: XCTestCase {
    
    var testManager: TestManager?
    
    override func setUp() {
        super.setUp()
        
        testManager = TestManager()
    }
    

    func testInvoke() throws {
        testManager?.invoke()
        
        assert(testManager?.isInvoked ?? false)
    }
}
