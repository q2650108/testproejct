//
//  ViewController.swift
//  TestProejct
//
//  Created by Foxconn_Civet_MacMini on 2021/10/20.
//

import UIKit

class ViewController: UIViewController {

    private let testManager = TestManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        testManager.invoke()
    }
}

