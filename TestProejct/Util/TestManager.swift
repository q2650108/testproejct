//
//  TestManager.swift
//  TestProejct
//
//  Created by Foxconn_Civet_MacMini on 2021/10/20.
//

import Foundation

class TestManager {
    var isInvoked = false
    
    func invoke() {
        isInvoked = true
    }
}
